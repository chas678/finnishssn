package com.pobox.cbarham;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.concurrent.ThreadLocalRandom;

import static com.pobox.cbarham.FinnishPersonalIdentityCode.forId;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
class FinnishPersonalIdentityCodeTest {
    @Test
    void idTokenIs_NotNull() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> forId(null));
        assertThat(ex.getMessage(), is("Id token was empty or null."));
    }

    @ParameterizedTest
    @ValueSource(strings = {"270274-885N", "010290+3581"})
    void validChecks(String validSSN) {
        assertThat(FinnishPersonalIdentityCode.valid(FinnishPersonalIdentityCode.forId(validSSN)), is(true));
    }

    @ParameterizedTest
    @ValueSource(strings = {"990274-885N", "A10290+3581"})
    void invalidChecks(String validSSN) {
        assertThat(FinnishPersonalIdentityCode.valid(FinnishPersonalIdentityCode.forId(validSSN)), is(false));
    }

    @RepeatedTest(1000)
    void creationWorks() {
        FinnishPersonalIdentityCode newone = FinnishPersonalIdentityCode.createWithAge(ThreadLocalRandom.current().nextInt(1, 150));
        log.warn("generated FinnishPersonalIdentityCode: {}", newone);
        assertTrue(FinnishPersonalIdentityCode.valid(newone), "Invalid: " + newone);
    }
}
package com.pobox.cbarham;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.time.Clock;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;
import static java.util.regex.Pattern.compile;

@Value
@ToString(onlyExplicitlyIncluded = true)
public class FinnishPersonalIdentityCode {
    private static final int DAY = 1;
    private static final int MONTH = 2;
    private static final int YEAR = 3;
    private static final int SEPARATOR = 4;
    private static final int CONTROL_NUMBER = 5;
    private static final int CONTROL_CHAR = 6;
    private static final int DIVIDER = 31;
    private static final Pattern IDNUMBER_PATTERN =
            compile("([0-3][0-9])([0-1][0-9])([0-9]{2})(\\-|[A]|\\+)([0-9]{3})([0-9]|[A-Z])");
    private static final Map<Integer, Character> centuryMap = ImmutableMap.of(2000, 'A', 1900, '-', 1800, '+');
    private static final Map<Character, Integer> centuryMapInverse = ImmutableBiMap.copyOf(centuryMap).inverse();
    private static final char[] CONTROL_CHARS = "0123456789ABCDEFHJKLMNPRSTUVWXY".toCharArray();
    @ToString.Include
    @EqualsAndHashCode.Include
    private final String idToken;
    static Clock clock = Clock.systemDefaultZone();

    private FinnishPersonalIdentityCode(String idToken) {
        if (Strings.nullToEmpty(idToken).trim().isEmpty()) {
            throw new IllegalArgumentException("Id token was empty or null.");
        }
        this.idToken = idToken.trim();
    }

    public static FinnishPersonalIdentityCode forId(String idToken) {
        return new FinnishPersonalIdentityCode(idToken);
    }

    public static FinnishPersonalIdentityCode createWithAge(int age) {
        Preconditions.checkArgument(age <= 150 && age >= 1, "age must be between 1 and 150 inclusive - was " + age);
        var today = LocalDate.now(clock).minusYears(age);
        var year = today.getYear();
        var month = Month.of(ThreadLocalRandom.current().nextInt(1, 13));
        var dayOfMonth = ThreadLocalRandom.current().nextInt(1, 1 + month.length(Year.isLeap(year)));
        var centurySign = centuryMap.get((year / 100) * 100);
        if (!birthDayPassed(LocalDate.of(year, month, dayOfMonth), today)) {
            year--;
        }
        year = year % 100;

        var rollingId = ThreadLocalRandom.current().nextInt(1, 801);
        var checkSumBase = Integer.parseInt(
                String.format("%02d%02d%02d%03d",
                        dayOfMonth,
                        month.getValue(),
                        year,
                        rollingId));
        var checksum = CONTROL_CHARS[checkSumBase % 31];

        return new FinnishPersonalIdentityCode(
                String.format("%02d%02d%02d%1s%03d%1s",
                        dayOfMonth,
                        month.getValue(),
                        year,
                        centurySign,
                        rollingId,
                        checksum));
    }


    public static boolean valid(final FinnishPersonalIdentityCode idNumber) {
        FinnishPersonalIdentityCode FinnishPersonalIdentityCode = forId(idNumber.getIdToken());
        Matcher format = IDNUMBER_PATTERN.matcher(FinnishPersonalIdentityCode.getIdToken());
        return format.matches()
                && FinnishPersonalIdentityCode.birthdayFor(format).isPresent()
                && FinnishPersonalIdentityCode.hasValidControl(format);
    }

    private static boolean birthDayPassed(LocalDate dateOfBirth, LocalDate today) {
        MonthDay birthday = MonthDay.from(dateOfBirth);
        MonthDay monthDay = MonthDay.from(today);
        return birthday.isBefore(monthDay);
    }


    private boolean hasValidControl(Matcher idFormat) {
        int controlNumber = parseInt(idFormat.group(DAY) + idFormat.group(MONTH) +
                idFormat.group(YEAR) + idFormat.group(CONTROL_NUMBER));
        return idFormat.group(CONTROL_CHAR).charAt(0) == CONTROL_CHARS[controlNumber % DIVIDER];
    }

    private Optional<LocalDate> birthdayFor(Matcher format) {
        try {
            return Optional.of(
                    LocalDate.of(
                            centuryFrom(format.group(SEPARATOR).charAt(0)) + parseInt(format.group(YEAR)),
                            parseInt(format.group(MONTH)),
                            parseInt(format.group(DAY))));
        } catch (DateTimeException e) {
            return Optional.empty();
        }
    }

    private int centuryFrom(char separator) {
        return Optional.ofNullable(centuryMapInverse.get(separator)).
                orElseThrow(() -> new IllegalArgumentException("Unknown century separator " + separator));
    }
}
